import numpy as np
import pandas as pd
import eli5
import string
import nltk
import joblib
from nltk.corpus import  stopwords
from sklearn.model_selection import cross_val_score
from nltk.stem import WordNetLemmatizer
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics import accuracy_score,classification_report,confusion_matrix,f1_score
## Import Data
train=pd.read_csv('Data/train.csv')
test=pd.read_csv('Data/test.csv')
    
def clean_text(train=train):    
    train['comment_text'] = train['comment_text'].str.lower()
    # Replace email addresses with 'email'
    train['comment_text'] = train['comment_text'].str.replace(r'^.+@[^\.].*\.[a-z]{2,}$','emailaddress')
    # Replace URLs with 'webaddress'
    train['comment_text'] = train['comment_text'].str.replace\
        (r'^http\://[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(/\S*)?$','webaddress')
    # Replace money symbols with 'moneysymb' (£ can by typed with ALT key + 156)
    train['comment_text'] = train['comment_text'].str.replace(r'£|\$', 'dollers')
    # Replace 10 digit phone numbers (formats include paranthesis, spaces, no spaces, dashes) with 'phonenumber'
    train['comment_text'] = train['comment_text'].str.replace\
        (r'^\(?[\d]{3}\)?[\s-]?[\d]{3}[\s-]?[\d]{4}$','phonenumber')  
    # Replace numbers with 'numbr'
    train['comment_text'] = train['comment_text'].str.replace(r'\d+(\.\d+)?', 'numbr')
    train['comment_text'] = train['comment_text'].apply(lambda x: ' '.join(term for term in x.split()\
        if term not in string.punctuation))
    stop_words = set(stopwords.words('english') + ['u', 'ü', 'ur', '4', '2', 'im', 'dont', 'doin', 'ure'])
    train['comment_text'] = train['comment_text'].apply(lambda x: ' '.join\
        (term for term in x.split() if term not in stop_words))
    lem=WordNetLemmatizer()
    train['comment_text'] = train['comment_text'].apply(lambda x: ' '.join\
        (lem.lemmatize(t) for t in x.split()))
clean_text(train)
train.head()

cols_target = ['malignant','highly_malignant','rude','threat','abuse','loathe']

tf_vec = TfidfVectorizer(max_features = 10000, stop_words='english')
features = tf_vec.fit_transform(train['comment_text'])

x = features
y=train[cols_target]


x_train,x_test,y_train,y_test=train_test_split(x,y,random_state=56,test_size=.30)

RF = RandomForestClassifier()
RF.fit(x_train, y_train)
y_pred_train = RF.predict(x_train)
print('Training accuracy is {}'.format(accuracy_score(y_train, y_pred_train)))
y_pred_test = RF.predict(x_test)
print('Test accuracy is {}'.format(accuracy_score(y_test,y_pred_test)))

cvs=cross_val_score(RF, x, y, cv=10, scoring='accuracy').mean()
print('cross validation score :',cvs*100)

y_pred_test = RF.predict(x_test)
print('Test accuracy is {}'.format(accuracy_score(y_test,y_pred_test)))

test_data =tf_vec.fit_transform(clean_text(test)['comment_text'])
test_data

eli5.show_weights(RF,vec = tf_vec, top = 15)

prediction = RF.predict(test_data)
test[cols_target] = prediction
test.to_csv('Data/submission.csv')

joblib.dump(RF,"RFModel.pkl")