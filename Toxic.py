import joblib
import pandas as pd

def label(y):
    labels=[]
    if y[:,0]==1:
        labels.append('Malignant')
        
    if y[:,1]==1:
        labels.append('Highly Malignant')
        
    if y[:,2]==1:
        labels.append('Rude')
        
    if y[:,3]==1:
        labels.append('Threat')
        
    if y[:,4]==1:
        labels.append('Abuse')
        
    if y[:,5]==1:
        labels.append('Loathe')
        
    if ((y[:,5]==0)&\
        (y[:,4]==0)&\
        (y[:,3]==0)&\
        (y[:,2]==0)&\
        (y[:,1]==0)&\
        (y[:,0]==0)):
        labels.append('Non Toxic')
        
    return labels

def toxic(text):
    train=pd.read_csv('Data/train.csv')
    from sklearn.feature_extraction.text import TfidfVectorizer
    tf_vec = TfidfVectorizer(max_features = 10000, stop_words='english')
    features = tf_vec.fit_transform(train['comment_text'])
    RF=joblib.load("Model/RFModel.pkl")
    test_data =RF.predict(tf_vec.transform([text]))
    return label(test_data)
